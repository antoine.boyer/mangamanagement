import React from "react";
import EditCreateManga from "../../../../components/editManga.component";
import { Manga } from "../../../../dto/mangas.model";


type IMangaDetailProps = {
    manga:Manga;
}


export default function EditManga(props:IMangaDetailProps){
    return(
        <main>
          <EditCreateManga manga={props.manga}></EditCreateManga>
            EDIT MANGA
        </main>
    )
}