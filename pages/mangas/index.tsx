import { useRouter } from "next/dist/client/router"
import React from "react";
import { Manga } from "../../dto/mangas.model";
import MangaCard from "../../components/mangaCard.component";


type IMangasProps = {
    mangas:Manga[];
}

export async function getStaticProps(context: any) {
    const res = await fetch(`http://localhost:3001/mangas`)
    const mangas : Manga[] = await res.json()
    if (!mangas) {
        return {
            redirect: {
                destination: '/',
                permanent: false,
            },
        }
    }
    return {
        props: { mangas }, // will be passed to the page component as props
    }
 }
export default function Mangas(props:IMangasProps){
    return(
        <main>
            <h1>List de manga</h1>
            <div style={{display:'flex', justifyContent: 'center', flexDirection:'column', alignItems:'center'}}>
            <button>back</button>
           {props.mangas&& <table>
            {props.mangas.map((manga:Manga,index:number)=>(
                <MangaCard manga={manga}></MangaCard>


            ))}
            </table>}
            </div>
        </main>
    )
}


 