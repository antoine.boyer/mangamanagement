import {Fragment, FunctionComponent, useEffect, useState} from 'react';
import { Manga } from '../dto/mangas.model';



type MangaCardProps={
     manga:Manga;
}



const MangaCard: FunctionComponent<MangaCardProps> = (props:MangaCardProps)=>{




    return(
        <div>

         <Fragment>

                     <div>
                          {props.manga.title} 
                          sortie le {props.manga.year} 
                           {props.manga.author&&<span>{props.manga.author.firstName+ props.manga.author.lastName}  </span>}
                           <a href={`/mangas/${props.manga.id}`}>
                              Detail
                           </a>
                            </div>

             </Fragment>

        </div>
       

    );
};
export default MangaCard;